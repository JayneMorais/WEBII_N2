CREATE DATABASE  IF NOT EXISTS `db_pswii` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `db_pswii`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: db_pswii
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `email`
--

DROP TABLE IF EXISTS `email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email` (
  `id_email` int(11) NOT NULL AUTO_INCREMENT,
  `data_hora` datetime DEFAULT NULL,
  `titulo` varchar(70) DEFAULT NULL,
  `corpo` varchar(100) DEFAULT NULL,
  `origem_id` int(11) NOT NULL,
  `destino_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_email`),
  KEY `id_origem_idx` (`origem_id`),
  CONSTRAINT `id_origem` FOREIGN KEY (`origem_id`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email`
--

LOCK TABLES `email` WRITE;
/*!40000 ALTER TABLE `email` DISABLE KEYS */;
INSERT INTO `email` VALUES (1,'2017-04-30 00:00:00','mens 1','mens 1 ...',1,2),(2,'2018-04-30 00:00:00','mens 2','mens 2 ...',2,1),(3,'2019-04-30 00:00:00','mens 3 ','mens 3 ...',3,2),(4,NULL,'mens 4 ','mens 4 ...',3,NULL),(6,'2017-05-01 08:00:52','we','we',1,2),(9,'2017-05-01 08:10:52','ei','ei',1,2),(10,'2017-05-01 09:25:22','mens 3 ','mens 3 ...',2,2),(11,'2017-05-01 09:25:32','mens 3 ','mens 3 ...',2,2),(12,'2017-05-01 09:25:40','mens 3 ','mens 3 ...',2,2),(14,'2017-05-01 09:26:09','mens 3 ','mens 3 ...',2,2),(15,'2017-05-01 22:57:55','puser2','hh',1,2),(16,'2017-05-01 22:58:47','pser22','ty',1,1),(19,'2017-05-02 06:22:05','gg','gg',1,4),(20,'2017-05-02 06:22:31','yy','yy',1,2),(21,'2017-05-02 06:23:10','qqqq','qqqqq',1,1),(23,'2017-05-02 06:25:12','1414','1414',1,2),(30,'2017-05-02 06:54:20','er','er',1,1),(32,'2017-05-02 07:47:26','11','11',1,1),(37,'2017-05-02 23:49:29','Jayne','tesjandjansjanjnajsdnajsnajndjasnjd',1,4),(39,'2017-05-02 23:51:21','teste','teste',1,1),(40,'2017-05-02 23:51:48','teste','teste',1,2),(41,'2017-05-02 23:55:36','er','er',1,2),(42,'2017-05-02 23:56:50','oi','oi',2,2),(43,'2017-05-02 23:57:05','3','3',2,1),(44,'2017-05-03 00:39:55','medo','medo',1,2);
/*!40000 ALTER TABLE `email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `email_remetente`
--

DROP TABLE IF EXISTS `email_remetente`;
/*!50001 DROP VIEW IF EXISTS `email_remetente`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `email_remetente` AS SELECT 
 1 AS `id_email`,
 1 AS `data_hora`,
 1 AS `titulo`,
 1 AS `corpo`,
 1 AS `origem_id`,
 1 AS `user_origem`,
 1 AS `destino_id`,
 1 AS `user_destino`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `user` varchar(25) DEFAULT NULL,
  `password` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'user1','user1'),(2,'user2','user2'),(3,'user3','user3'),(4,'ee','ee'),(26,'a','0cc175b9c0f1b6a');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `view1`
--

DROP TABLE IF EXISTS `view1`;
/*!50001 DROP VIEW IF EXISTS `view1`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view1` AS SELECT 
 1 AS `id_email`,
 1 AS `data_hora`,
 1 AS `titulo`,
 1 AS `corpo`,
 1 AS `origem_id`,
 1 AS `user_origem`,
 1 AS `destino_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `email_remetente`
--

/*!50001 DROP VIEW IF EXISTS `email_remetente`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `email_remetente` AS select `view1`.`id_email` AS `id_email`,`view1`.`data_hora` AS `data_hora`,`view1`.`titulo` AS `titulo`,`view1`.`corpo` AS `corpo`,`view1`.`origem_id` AS `origem_id`,`view1`.`user_origem` AS `user_origem`,`view1`.`destino_id` AS `destino_id`,`usuario`.`user` AS `user_destino` from (`view1` join `usuario`) where (`usuario`.`id_usuario` = `view1`.`destino_id`) order by `view1`.`id_email` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view1`
--

/*!50001 DROP VIEW IF EXISTS `view1`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view1` AS select `email`.`id_email` AS `id_email`,`email`.`data_hora` AS `data_hora`,`email`.`titulo` AS `titulo`,`email`.`corpo` AS `corpo`,`email`.`origem_id` AS `origem_id`,`usuario`.`user` AS `user_origem`,`email`.`destino_id` AS `destino_id` from (`email` join `usuario`) where (`email`.`origem_id` = `usuario`.`id_usuario`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-02 18:03:00
