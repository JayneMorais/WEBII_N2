<?php
class post {

    private $id;
    private $texto;
    private $data;
    private $usuario_id;
    
    function getId() {
        return $this->id;
    }

    function getTexto() {
        return $this->texto;
    }

    function getData() {
        return $this->data;
    }

    function getUsuario_id() {
        return $this->usuario_id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTexto($texto) {
        $this->texto = $texto;
    }

    function setData($data) {
        $this->data = $data;
    }

    function setUsuario_id($usuario_id) {
        $this->usuario_id = $usuario_id;
    }



}
