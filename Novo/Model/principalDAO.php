<?php
	require("../config.php");
        require("banco.php");
        
	class principalDAO extends banco
	{
      
            function criarTimeline($id) {
                $sql = $this->db->prepare("SELECT id,nome,foto,conteudo, data FROM postagem,usuario WHERE usuario_id = idusuario AND (usuario_id = ? OR usuario_id IN "
                        . "(SELECT amigo_id FROM contato WHERE usuario_id=?)) ORDER BY id DESC");

                $sql->execute(array($id,$id));
          
                if($sql->rowCount()>0){
                    return $sql->fetchAll();
                }
            }
            
            function verificarUltimaPostagem($id) {
                $sql = $this->db->prepare("SELECT id, data FROM postagem WHERE usuario_id=? ORDER BY id DESC");
                $sql->execute(array($id));
                
                if($sql->rowCount()>0){
                    return $sql->fetchAll();
                }
                
                
            }
            function inserirPostagem($id, $texto, $dataAtual) {
                $sql= $this->db->prepare("INSERT INTO postagem (conteudo, data, usuario_id) VALUES (?,?,?)");
                $sql->execute(array($texto,$dataAtual,$id['idusuario']));
                
                $sql = $this->db->prepare("SELECT MAX(id) as maximo FROM postagem");
                $sql->execute();
                $postagem = $sql->fetch();
                
                $listaAmigos = $this->listaNotificar($id);
                if($listaAmigos != false){
                    foreach ($listaAmigos as $amigos){
                        $sql=$this->db->prepare("INSERT INTO notificacao (postagem_id, amigo_id, visualizada) VALUES (?,?,0)");
                        $sql->execute(array($postagem['maximo'], $amigos['usuario_id']));
                        }
                    }
                }
                
                
                
            
            
            function buscarNotificacoes($id) {
                $sql = $this->db->prepare("Select nome, conteudo from postagem,usuario where idusuario =usuario_id AND id in "
                        . "(select postagem_id FROM notificacao WHERE amigo_id=? AND visualizada=0)");
                $sql->execute(array($id));
                
                if($sql->rowCount()>0){
                    return $sql->fetchAll();
                }else{
                    return false;
                }
            }
            
            function visualizarNotificacoes($id){
                $sql = $this->db->prepare("UPDATE notificacao SET visualizada=1 where amigo_id=?");
                $sql->execute(array($id));
            }
            
            function listaNotificar($id) {
                $sql = $this->db->prepare("SELECT usuario_id FROM contato WHERE amigo_id=?");
                $sql->execute(array($id['idusuario']));
                
                if($sql->rowCount()>0){
                    return $sql->fetchAll();
                }else{
                    return false;
                }
            }

	

        }


?>