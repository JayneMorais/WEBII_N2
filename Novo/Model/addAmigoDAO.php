<?php

require("banco.php");

class addAmigoDAO extends banco {

    function addUser($id,$email) {
        $user = array();
        $sql = $this->db->prepare("SELECT idusuario FROM usuario WHERE email=?");
        $sql->execute(array($email));

        if ($sql->rowCount() > 0) {
            $amigo = $sql->fetch();
            $sql= $this->db->prepare('INSERT INTO contato(usuario_id,amigo_id) values (?,?)');
            $sql->execute(array($id['idusuario'],$amigo['idusuario']));
            if($sql->rowCount()>0){
                return 1;
            }else{
                return -1;
            }
        } else {
            return -1;
        }
    }

}
?>
