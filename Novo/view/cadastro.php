<head>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../assets/css/meuestilo.css">
  <link href="../assets/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="https://fonts.googleapis.com/css?family=Amatic+SC|Julee|Kaushan+Script|Pacifico" rel="stylesheet">

<script
  src="http://code.jquery.com/jquery-3.2.1.js"
  integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
  crossorigin="anonymous"></script>
    <script type="text/javascript" src="../assets/js/materialize.min.js"></script>
</head>

<body>
<ul id="slide-out" class="side-nav fixed z-depth-2 lime lighten-5">

    <li class="center no-padding">
      <div class="lime darken-2 white-text" style="height: 180px;">
        <div class="row">
          <img style="margin-top: 5%;" src="../assets/images/logo.png" alt=""  width="100" height="100" class="circle responsive-img valign profile-image-login">

          <p class="center login-form-text "><h5 style=" margin-top: 5%; color:tomato ; font-weight: bold; font-size:38px; text-shadow: 1px 0px 0px black, 
                 -1px 0px 0px black, 
                 0px 1px 0px black, 
                 0px -1px 0px black; font-family:'Julee',cursive;""> Ultra</h5></p>
          </div>

        </div>
      </div>
    </li>


  </ul>
        

  <header>
    <ul class="dropdown-content" id="user_dropdown">
      <li><a class="lime-text" href="#!">Logout</a></li>
    </ul>

    <nav class="lime" role="navigation">
      <div class="nav-wrapper">
        <a style="margin-left: 20px;" class="breadcrumb" href="#!">Bem vindo!</a>

        <ul class="right hide-on-med-and-down">
          <li>
            <a class='right dropdown-button' href='' data-activates='user_dropdown'><i class=' material-icons'>exit_to_app</i></a>
          </li>
        </ul>

        <a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
      </div>
    </nav>

    <nav>
      <div class="nav-wrapper lime darken-2">
        <a style="margin-left: 20px;" class="breadcrumb" href="#!">Ultra</a>
        <a class="breadcrumb" href="#!">Cadastro</a>

        <div style="margin-right: 20px;" id="timestamp" class="right"></div>
      </div>
    </nav>
  </header>


  
  <main class="col s6 m6 xl6">
     
    
  <div class="row"> 
    <div class="col s8 z-depth-4 offset-s1 lime lighten-5" style="margin:6%;">
      <div class="row"></div>

      <form action="../Controller/cadastrarController.php" method="post" enctype="multipart/form-data" name="form1">

        <table class="centered">
          <tr> 

            <td>Nome</td>
            <td><input type="text" name="nome" required></td>
         <tr>
            <td>Email</td>
            <td><input type="email" name="email" type="text"  id="email" required></td>
         </tr>
          <tr> 
            <td>Senha</td>
            
             <td><input type="password" name="senha" id="senha"></td>
            
            
          </tr>
          <tr> 
            <td>Foto</td>
            
            <td><input type="file" name="foto" id="foto" required></td>
            <td><input type="submit" value="Cadastrar"></td>
                        
          </tr>
    

        </table>
      </form>
    </div>
  </div>

  </main>

  <footer class="lime page-footer">
    <div class="container">
      <div class="row">
        <div class="col s12">
          <h5 class="white-text">Ultra</h5>
          <ul id='credits'>
            <li>
              Projeto da Disciplina de WEB II 
            </li>
            <li>
              Jayne Morais e Iago Franco
            </li>
            <li>
              Agosto, 2017
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
        <span style="color: grey">Instituto Federal de Educação, Ciência e Tecnologia do Ceará - campus Crato.</span>
      </div>
    </div>
  </footer>
</body>



<script type="text/javascript">

console.log("adasd");

  $(document).ready(function(){
     // Initialize collapse button
    $(".button-collapse").sideNav();
    // Initialize collapsible (uncomment the line below if you use the dropdown variation)
    $('.collapsible').collapsible();

    $('select').material_select();


    
  });

</script>

</html>


