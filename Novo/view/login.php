
<!DOCTYPE html>
<html lang="pt-BR">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta charset="UTF-8">

  <title>Login Page</title>

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Amatic+SC|Julee|Kaushan+Script|Pacifico" rel="stylesheet">
  
  <link href="../assets/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  
  
</head>

<body class="lime">
  <!-- Início-->
  <div id="loader-wrapper">
    <div id="loader"></div>        
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
  </div>
  <!-- Final -->


  <div class="row"></div>
  <div id="login-page" class="row" >

    
    <div class="col s4 z-depth-4 card-panel offset-s4  lime lighten-5" >

        <form class="login-form" method="POST" action="../Controller/loginController.php">
        <div class="row ">
          <div class="input-field col s12 center">
            <img src="../assets/images/logo.png" alt=""  width="180" height="180" class="circle responsive-img valign profile-image-login">
            <p class="center login-form-text "><h5 style="color:tomato ; font-weight: bold; text-shadow: 1px 0px 0px black, 
                 -1px 0px 0px black, 
                 0px 1px 0px black, 
                 0px -1px 0px black; font-size:40px; font-family:'Julee',cursive;""> Ultra</h5></p>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-social-person-outline prefix "></i>
            <i class="material-icons prefix red-text">account_circle</i>
            <input id="user" type="email" name="email">
            <label for="user" class="center-align">Email</label>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix "></i>
            <i class="material-icons prefix red-text">lock</i>
            <input id="password" type="password" name="senha">
            <label for="password">Password</label>
          </div>
        </div>
        
    

            <div class="input-field col s6 m6 l6">
            <button class="btn red waves-effect waves-light white-text" type="submit" name="Login">Login
              <i class="material-icons right ">send</i>
          </div>          

        <div class="row">
          <div class="input-field col s8 m6 l6">
              <p class="margin medium-small"><a href="cadastro.php">Cadastre-se</a></p>
          </div>         
        </div>

      </form>
                </div>

    </div>
  </div>

  <script
  src="http://code.jquery.com/jquery-3.2.1.js"
  integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
  crossorigin="anonymous"></script>
  <script type="text/javascript" src="../assets/js/materialize.min.js"></script>

  
</body>

</html>