<head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../assets/css/meuestilo.css">
    <link href="../assets/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC|Julee|Kaushan+Script|Pacifico" rel="stylesheet">

    <script
        src="http://code.jquery.com/jquery-3.2.1.js"
        integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
    crossorigin="anonymous"></script>
    <script type="text/javascript" src="../assets/js/materialize.min.js"></script>
     <script type="text/javascript">

        function exibirToast(){
            Materialize.toast(arguments[0]+":<br/>"+arguments[1]+"...",4000,'rounded');
        }
        </script>
</head>

<body>
    <ul id="slide-out" class="side-nav fixed z-depth-2 lime lighten-5">

        <li class="center no-padding">
            <div class="lime darken-2 white-text" style="height: 180px;">
                <div class="row">
                    <img style="margin-top: 5%;" src="../assets/images/logo.png" alt=""  width="100" height="100" class="circle responsive-img valign profile-image-login">

                    <p class="center login-form-text "><h5 style=" margin-top: 5%; color:tomato ; font-weight: bold; font-size:38px; text-shadow: 1px 0px 0px black, 
                                                           -1px 0px 0px black, 
                                                           0px 1px 0px black, 
                                                           0px -1px 0px black; font-family:'Julee',cursive;""> Ultra</h5></p>
                </div>

            </div>
            </div>
        </li>

        <ul class="collapsible" data-collapsible="accordion">
            <li id="users_seller">
                <a class="waves-effect collapsible-header" style="text-decoration: none;" href="../Controller/principalController.php"><b>Timeline</b></a>
            </li>
            <li id="users_seller">
                <a class="waves-effect collapsible-header" style="text-decoration: none;" href="../Controller/perfilController.php"><b>Meu Perfil</b></a>
            </li>
            <li id="users_seller">
                <a class="waves-effect collapsible-header" style="text-decoration: none;" href="../Controller/addAmigoController.php"><b>Meus Amigos</b></a>
            </li>

        </ul>
    </ul>


    <header>
        <ul class="dropdown-content" id="user_dropdown">
            <li><a class="lime-text" href="../index.php">Logout</a></li>
        </ul>

        <nav class="lime" role="navigation">
            <div class="nav-wrapper">
                <a style="margin-left: 20px;" class="breadcrumb" href="#!">Bem vindo!</a>
                <a data-activates="slide-out" class="button-collapse show-on-" href="#!"><img style="margin-top: 17px; margin-left: 5px;" src="https://res.cloudinary.com/dacg0wegv/image/upload/t_media_lib_thumb/v1463989873/smaller-main-logo_3_bm40iv.gif" /></a>

                <ul class="right hide-on-med-and-down">
                    <li>
                        <a class='right dropdown-button' href='../Controller/logout.php' data-activates='user_dropdown'><i class=' material-icons'>exit_to_app</i></a>
                    </li>
                </ul>

                <a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
            </div>
        </nav>

        <nav>
            <div class="nav-wrapper lime darken-2">
                <a style="margin-left: 20px;" class="breadcrumb" href="#!">Ultra</a>
                <a class="breadcrumb" href="#!">Home</a>

                <div style="margin-right: 20px;" id="timestamp" class="right"></div>
            </div>
        </nav>
    </header>



    <main class="col s6 m6 xl6">

        <div style="padding-top: 20px;" class="row">

            <div class="col s12 m7 offset-m2">

                <div class="card horizontal"> 
                    <form method="POST" action="../Controller/postsController.php" class="col s12">
                        <div class="row">
                            <div class="input-field col s12">
                                <textarea  class="materialize-textarea" maxlength="200" length="200" id="conteudo" name="conteudo" rows="3" cols="25" wrap="soft"> </textarea>
                                <label for="conteudo">Post</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col 3">
                                <input style="margin-left: 20%; " type="submit" value="Postar">

                            </div>
                        </div>

                    </form>
                </div>

            </div>
            <?php
            if ($notificacoes == false) {
            } else {
                foreach ($notificacoes as $notifcs){
                    echo "<script type=\"text/javascript\">".
                        "exibirToast('".$notifcs['nome']."','".substr($notifcs['conteudo'], 0,20)."');".
                        "</script>";
                }
                
                }
            

            if ($posts == null) {
                
            } else {
                foreach ($posts as $post) {
                    echo " <div class=\"col s12 m7 offset-m2\">
               <div class=\"card horizontal\"> 
                   <div class=\"row\">
                     <img style=\"margin-top: 50%; margin-left: 20%; \" src=\"../assets/images/" . $post['foto'] . "\"  width=\"70\" height=\"70\" class=\"circle responsive-img valign profile-image-login\">
                   </div>



                 <div class=\"card-stacked\">

                   <div class=\"card-content\">
                     <p style=\"margin-right: 0%; margin-left: 10%;\"><i>" . $post['conteudo'] . "<i></p>
                   </div>

                   <div class=\"card-action\">


                     <div class=\"row\" style=\"margin-right: 20%; margin-left: 50%;\">
                        <p align=\"right\"><b>" . $post['nome'] . "</b></p>
                        <p align=\"right\"><i>" . $post['data'] . "<i></p>
                     </div>

                   </div>


                 </div>

               </div>
             </div>";
                }
            }
            ?>




    </main>
    <footer class="lime page-footer">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h5 class="white-text">Ultra</h5>
                    <ul id='credits'>
                        <li>
                            Projeto da Disciplina de WEB II 
                        </li>
                        <li>
                            Jayne Morais e Iago Franco
                        </li>
                        <li>
                            Agosto, 2017
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                <span class="white-text">Instituto Federal de Educação, Ciência e Tecnologia do Ceará - campus Crato.</span>

            </div>
        </div>
    </footer>



    <script type="text/javascript">


        $(document).ready(function () {

            // Initialize collapse button
            $(".button-collapse").sideNav();
            // Initialize collapsible (uncomment the line below if you use the dropdown variation)
            $('.collapsible').collapsible();

            $('select').material_select();
            
            


        });

    </script>

</html>


