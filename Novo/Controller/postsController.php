<?php

session_start();
require("../Model/principalDAO.php");


$id = $_SESSION['idusuario'];

if (isset($_POST['conteudo']) && !empty($_POST['conteudo'])) {
    $dataAtual = date("d-m-Y");
    $DAO = new principalDAO();
    $dataAnt = $DAO->verificarUltimaPostagem($id['idusuario']);
    if ($dataAnt[0]['data'] != $dataAtual) {
        $texto = addslashes($_POST['conteudo']);
        $DAO->inserirPostagem($id, $texto, $dataAtual);
         echo "<script>alert('Postagem feita!');window.location.href='principalController.php'</script>";
    } else {
        echo "<script>alert('Volte novamente amanha para uma nova postagem!');window.location.href='principalController.php'</script>";
    }
} else {
    header('Location: principalController.php');
}
?>