<?php
session_start();
require("../Model/addAmigoDAO.php");
require("controller.php");

if(isset($_POST['email']) && !empty($_POST['email'])){
    $email = addslashes($_POST['email']);
    $id = $_SESSION['idusuario'];
    $dao = new addAmigoDAO();
    $retorno = $dao->addUser($id, $email);
    if($retorno==1){
          echo "<script>alert('Amigo adicionado!');window.location.href='principalController.php'</script>";
    }else{
          echo "<script>alert('Usuario nao existe OU ja eh seu amigo!');window.location.href='addAmigoController.php'</script>";
    }
    
}

class addAmigoController extends controller{
    
    function carregarTela() {
        $this->loadView('amigos.php');
    }
    
}

$controller = new addAmigoController();
$controller->carregarTela();



?>